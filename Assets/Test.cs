﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Test : MonoBehaviour {
    public float maxS = 11f;
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    private bool muerto;
    private Animator anim;
    private bool flipped = false;
    public bool proba;
    private bool atack=false;
   public bool jump = true;
    public AudioSource audio;
    public AudioSource kabom;
    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    void FixedUpdate()
    {
        if (muerto)
            return;
        move = Input.GetAxis("Horizontal");
        rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
      
        if(rb2d.velocity.x > 0.00001f || rb2d.velocity.x < -0.00001f)
        {
            if((rb2d.velocity.x < -0.0001f && !flipped) || (rb2d.velocity.x > -0.0001f && flipped))
            {
                flipped = !flipped;
                this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
            }
            anim.SetBool("walk", true);
        }
        else
        {
            anim.SetBool("walk", false);
        }
        if(Input.GetButtonDown("Jump"))
        {
            if (jump == true)
            {
                audio.Play();
                rb2d.AddForce(new Vector2(0, 300));
                anim.SetBool("salto", true);
            }
            else
                rb2d.AddForce(new Vector2(0, 0));
        }
        if (Input.GetKeyDown(KeyCode.X)){
            anim.SetBool("attack", true);
        }
        else
        anim.SetBool("attack", false);
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "box")
        {
            muerto = true;
            anim.SetTrigger("Death");
            kabom.Play();
            Invoke("Over", 2);
            Debug.Log("Ostia");
        }
        if (coll.gameObject.tag == "floor")
        {
            jump = true;
            anim.SetBool("salto", false);
        }
        if (coll.gameObject.tag == "serra")
        {
            kabom.Play();
            Invoke("Over", 2);
            
            muerto = true;
            anim.SetTrigger("Death");
            Debug.Log("Ostia");
        }
        if (coll.gameObject.tag == "secret")
        {
            SceneManager.LoadScene("Sec");  
        }
        if (coll.gameObject.tag == "goal")
        {
            SceneManager.LoadScene("Victory");
        }
    }
    private void OnCollisionExit2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "floor")
        {
            jump = false;
        }
    }

    public void showmessage()
    {
        Debug.Log("Mensaje lanzado");
    }
    public void Over()
    {
        SceneManager.LoadScene("GameOver");
    }
}
