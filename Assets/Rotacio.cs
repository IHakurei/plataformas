﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotacio : MonoBehaviour
{
    private float rotacio;
    public float move = 0.03f;


    void Update()
    {
        rotacio += 2;
        transform.rotation = Quaternion.Euler(0, 0, rotacio);
       
    }
   
}
