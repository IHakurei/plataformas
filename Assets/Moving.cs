﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving : MonoBehaviour {
    public float timing = 0f;
    public float max;
    public float speed;
    private bool avançar=true;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
     
        if (timing > 101.0f)
            avançar = false;
        if (avançar)
        {
            transform.Translate(speed * Time.deltaTime, 0, 0);
            timing += 1.0f;
        }
        else
        {
            timing -= 1.0f;
            transform.Translate(-speed * Time.deltaTime, 0, 0);
            if (timing <= 0)
                avançar = true;
        }


    }

}
